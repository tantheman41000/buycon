import React, { Component } from 'react'
import {View, Text,StyleSheet,TouchableOpacity,Alert} from 'react-native'
import { Input, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class AddBeacon extends Component {

    //FOR ALERT ----------------------------------------------
    buttonClickded = () => {
        Alert.alert(
          "You Beacon is all set",
          "Would you like to broadcast  your beacon now",
          [
            {
              text: "Edit",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "YEs", onPress: () => this.props.navigation.navigate('Choise') }
          ],
          { cancelable: false }
        );
      };
      //FOR ALERT ----------------------------------------------

    render() {
        return (
            <View style = {styles.container}>
                 <Text style = {{fontSize : 20, justifyContent: "space-between", marginBottom : 20}} >Add Restaurant's information</Text>
                 <Input
                                     
                    placeholder=' Beacon name'
                    leftIcon={
                    <Icon
                    name='home'
                    size={24}
                    color='grey'
                    />    
                    }
                />
                <Input
                    placeholder=' UUID'
                    leftIcon={
                    <Icon
                    name='signal'
                    size={24}
                    color='grey'
                    />    
                    }
                    />
                    <Input
                    placeholder=' Minor'
                    leftIcon={
                    <Icon
                    name='feed'
                    size={24}
                    color='grey'
                    />    
                    }
                    />

                <Text style = {{padding : 25,height: 50}}/>
                
                <TouchableOpacity
                 style = {styles.button}
                 onPress = {this.buttonClickded}
                >
                    <Text style={{color:'white',fontSize: 20}}>
                    <Icon
                    name="plus"
                    size={30}
                    color="#e5e5e5"
                    /> 
                    register my beacon
                    </Text>
                    </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
        justifyContent: 'space-between'
      },
    button: {
        backgroundColor: '#2089DC', width: "50%", borderRadius: 12, justifyContent: 'center', alignItems: 'center',paddingVertical: 10
    },
});
