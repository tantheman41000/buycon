import React, { Component } from 'react'
import {View, Text, Alert,StyleSheet} from 'react-native'
import { Input, Button, Card} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';

import MenuCard from '../card/MenuCard'

export default class InputRestaurant extends Component {
    

    //FOR ALERT ----------------------------------------------
    buttonClickded = () => {
        Alert.alert(
          "You restaurant is all set",
          "Would you like to set up  your beacon now",
          [
            { text: "Later", onPress: () => console.log("later pressed") },
            {
              text: "Edit",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "YEs", onPress: () => this.props.navigation.navigate('adddBeacon') }
          ],
          { cancelable: false }
        );
      };
      //FOR ALERT ----------------------------------------------



    render() {
        return (
            <View  style = {styles.container}>
                <Text style = {{textAlign:'center', padding : 10, fontSize: 20}} >INPUT DISH</Text>
                                     <Input
                                     
                                            placeholder=' dish name'
                                            leftIcon={
                                            <Icon
                                            name='cutlery'
                                            size={24}
                                            color='grey'
                                            />    
                                        }
                                     />
                                      <Input
                                            placeholder=' price'
                                            leftIcon={
                                            <Icon
                                            name='usd'
                                            size={24}
                                            color='grey'
                                            />    
                                        }
                                     />
                                     
                                      <Input
                                      style={{
                                        borderBottomColor: '#000000',
                                        borderBottomWidth: 1 }}
                                            placeholder=' description'
                                            leftIcon={
                                            <Icon
                                            name='plus-circle'
                                            size={24}
                                            color='grey'
                                            />    
                                        }
                                     />
                                     <Text style = {{padding : 25,height: 50}}/>
                                      <Icon
                                            
                                            name='upload'
                                            size={50}
                                            color='grey'
                                            style = {{textAlign:'center', padding : 10}}
                                            > <Text style = {{textAlign:'auto', padding : 10 , fontSize : 15, marginBottom: 5}}>  upload photo </Text></Icon>  

                                        <Button
                                        
                                        style = {{padding : 25,height: 50}}
                                        icon={
                                            <Icon
                                            name="plus-circle"
                                            size={15}
                                            color="white"
                                            />
                                        }
                                        iconRight
                                        title="ADD MORE  "
                                        />

                                       
                                            <MenuCard/>
                                            <Text style = {{padding : 25,height: 25}}/>
                                        

                                        <Button
                                        onPress = {this.buttonClickded}
                                        style = {{padding : 25}}
                                        icon={
                                            <Icon
                                            name="save"
                                            size={15}
                                            color="white"
                                            />
                                        }
                                        iconRight
                                        title="SAVE  "
                                        />

                                        

                                     
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
        justifyContent: 'space-between'
      },
    button: {
        backgroundColor: '#2089DC', width: "50%", borderRadius: 12, justifyContent: 'center', alignItems: 'center',paddingVertical: 10
    },
});