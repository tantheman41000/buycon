import React, { Component } from 'react'
import {View, Text,StyleSheet, Modal, TouchableHighlight, Alert} from 'react-native'
import { Input, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Geocoder from 'react-native-geocoding';

export default class InputRestaurant extends Component {
    state = {
        modalVisible: false,
      };

      //FOR MODAL ----------------------------------------------
      setModalVisible(visible) {
        this.setState({modalVisible: visible});
      }
      Submit(){
        this.setModalVisible(!this.state.modalVisible)
      }
      //FOR MODAL ----------------------------------------------
      //FOR ALERT ----------------------------------------------
      buttonClickded = () => {
        Alert.alert(
          "Welcome to Buycon",
          "Restaurant information HERE",
          [
           
            {
              text: "Edit",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "YEs", onPress: () => this.props.navigation.navigate('AdDish') }
          ],
          { cancelable: false }
        );
      };
      //FOR ALERT ----------------------------------------------
      //FOR NAVIGOATION ----------------------------------------
      navigationToDishInput() {
        this.props.navigation.navigate('AdDish')
      }
      //FOR NAVIGOATION ----------------------------------------

      onPressCombine(){
        this.buttonClickded;
      }
    constructor(){
        super();
        this.state = {
            ready: false,
            where: {lat:null, lng:null},
            error: null
        }
    }

    componentDidMount(){
        let geoOptions = {
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 60*60*24
        };
        this.setState({ready:false, error: null});
        navigator.geolocation
    }

    geoSuccess = (position) => {
        this.setState({ready:true})
    }

    geoFailure = (err) =>{
        this.setState({error: err.message});
    }


    getData(){
        Geocoder.init("AIzaSyCjZ8rGQvLmED8pOu17K30TuQqcgi5x8xo");

        Geocoder.from(41.89, 12.49)
		.then(json => {
        		var addressComponent = json.results[0].address_components[0];
            console.log(addressComponent);
            alert(addressComponent.long_name)
		})
		.catch(error => console.warn(error));
    }
    render() {
        return (
            <View style = {styles.container}>
                <Text style = {{fontSize : 20, justifyContent: "space-between", marginBottom : 20}} >Add Restaurant's information</Text>
                                     <Input
                                     
                                            placeholder=' Restaurant name'
                                            leftIcon={
                                            <Icon
                                            name='home'
                                            size={24}
                                            color='grey'
                                            />    
                                        }
                                     />
                                     <Input
                                            
                                            placeholder=' Description'
                                            leftIcon={
                                            <Icon
                                            name='plus'
                                            size={24}
                                            color='grey'
                                            />    
                                        }
                                    />
                                    <Text style = {{padding : 25,height: 50}}/>
                                      <Icon
                                            
                                            name='upload'
                                            size={50}
                                            color='grey'
                                            style = {{textAlign:'center', padding : 10}}
                                            > 
                                            <Text style = {{textAlign:'auto', padding : 10 , fontSize : 15, marginBottom: 5, height: 20} }>  upload photo </Text></Icon>  
                                        {/* GetRestaurant location */}

                                        {/* <View>
                                            {!this.setState.ready && (
                                            <Text> this is the location</Text>
                                            )}
                                            {this.state.error && (
                                                <Text>{this.state.error}</Text>
                                            )}
                                            { this.state.ready && (
                                                <Text>{
                                                    `Lat : ${this.state.where.lat}
                                                    Lng : ${this.state.where.lng}`
                                                }
                                                </Text>
                                            )}
                                        </View> */}

                                        <Button
                                        onPress={()=>{this.getData()}}
                                        icon={
                                            <Icon
                                            name="map"
                                            size={15}
                                            color="white"
                                            />
                                        }
                                        iconRight
                                        title="GET LOCATION  "
                        
                                        /> 
                                    <Text style = {{padding : 25,height: 50}}/>
                                    <Button
                                        onPress = {this.buttonClickded}
                                        icon={
                                            <Icon
                                            name="unlock"
                                            size={15}
                                            color="white"
                                            
                                            />
                                        }
                                        iconRight
                                        title="NEXT  "
                                        />
                                                                            
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
        justifyContent: 'space-between'
      },
    button: {
        backgroundColor: '#2089DC', width: "50%", borderRadius: 12, justifyContent: 'center', alignItems: 'center',paddingVertical: 10
    }
});