import React, { Component } from 'react'
import {View, Text, TextInput, Image,StyleSheet, TouchableOpacity} from 'react-native'
import { Input, Button, Card} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';


export default class Chooser extends Component {
    render() {
        return (
            <View>
            <View style={styles.container}>
                 <TouchableOpacity
                 style = {styles.button}
                 onPress={() => this.props.navigation.navigate('RgRest')}
                ><Text style={{color:'white',fontSize: 20}}>
                    <Icon
                    name="plus"
                    size={30}
                    color="#e5e5e5"
                    /> 
                     Store
                    </Text>
                    </TouchableOpacity>
                    <Text style = {{padding : 10,height: 25}}/>
                    <TouchableOpacity
                     style = {styles.button}
                     onPress={() => this.props.navigation.navigate('adddBeacon')}
                ><Text style={{color:'white',fontSize: 20}}>
                    <Icon
                    name="plus"
                    size={30}
                    color="#e5e5e5"
                    /> 
                     Beacon
                    </Text>
                    </TouchableOpacity>
            </View>
            <Text style = {{padding : 25,height: '65%'}}/>
                <View style = {styles.container}>
                    <TouchableOpacity
                            style = {styles.buttonOut}
                            onPress={() => this.props.navigation.navigate('LoginRoute')}
                        ><Text style={{color:'white',fontSize: 20}}>
                            Log out
                            </Text>
                            </TouchableOpacity>
                </View>
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
        justifyContent: 'space-between'
      },
    button: {
        backgroundColor: '#2089DC', width: "50%", borderRadius: 12, justifyContent: 'center', alignItems: 'center',paddingVertical: 10
    },
    buttonOut: {
        backgroundColor: 'grey', width: "50%", borderRadius: 12, justifyContent: 'center', alignItems: 'center',paddingVertical: 10
    }
});