import React, { Component } from 'react'
import {View, Text, TextInput,StyleSheet, Touchable} from 'react-native'
import { Input, Button, Card} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { withNavigation } from 'react-navigation'
import * as Animatable from 'react-native-animatable';

 class LoginPage extends Component {

   
    render() {
        return (
            <View >
                <Text style = {{padding : 25,height: 30}}/>
                <Text style = {{textAlign:'center', padding : 10, fontSize: 20}} >Welcome to Buycon</Text>

                <Input
                    placeholder=' User'
                    leftIcon={
                    <Icon
                    name='user'
                    size={24}
                    color='grey'
                    />    
                }
                />

                <Input
                    placeholder=' Password'
                    leftIcon={
                    <Icon
                    name='key'
                    size={24}
                    color='grey'
                    />    
                }
                />

                <Text style = {{padding : 25,height: 50}}/>

              
               
                <Button
                onPress = {() =>this.props.navigation.navigate('Choise')}
                Hover
                title=" Login"
                type="outline"
                 style = {{padding : 25}}
                />
                
                
                <Text style = {{height: 10}}/>
                 <Button
                 onPress={() => this.props.navigation.navigate('CreateAccount')}
                title=" Register"
                type="outline"
                 style = {{padding : 25}}
                />
                
            </View>
        )
    }
}
export default withNavigation(LoginPage)