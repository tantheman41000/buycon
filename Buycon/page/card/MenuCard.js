import React, { Component } from 'react'
import { View, Text, Image ,TouchableHighlight} from 'react-native'
import { Card, Button, Icon,List, ListItem} from 'react-native-elements'
import axios from 'axios';


const list = []

export default class componentName extends Component {

  state = {
    Dish: []
  }

  componentDidMount() {
    const url = 'http://192.168.1.7:8080/dish/thai%20smile'
    axios.get(url)
    .then(res => {
      const Dish = res.data;
      this.setState({ Dish });
    })
  }

  renderRestaurant(){
    
    return this.state.Dish.map(dish => <Text>{Dish.dishes}</Text>);
    
  }

    render() {
        return (
            <View style = {{borderBottomColor: 'grey'}}>
             
                  
                    {this.state.Dish.map(data =>
                    <Card>
                          <View style={{ flexDirection: 'row'}} >
                                <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} >
                            <Image
                              
                              style={{width:50,height:50}}
                              resizeMode="cover"
                              source={ {uri :data.image }}
                            
                            />
                            </View>
                            <View style={{width: 100, height: 50, justifyContent: 'center', alignItems: 'center'}} >
                              <TouchableHighlight style = {{backgroundColor: 'grey', padding : 10}}><Text style = {{color : 'white'}}>{data.dishName}</Text></TouchableHighlight></View>
                            <View style={{width: 100, height: 50, justifyContent: 'center', alignItems: 'center'}} ><Text >{data.price}</Text></View>
                            <View style={{width: 100, height: 50, justifyContent: 'center', alignItems: 'center'}} ><Text >{data.dishDesc}</Text></View>
                          </View>
                      </Card>
                    )}
             </View>
        )
    }
}
