import React, { Component } from 'react'
import {View, Text, TextInput,StyleSheet} from 'react-native'
import { Input, Button, Card,FormInput} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Register extends Component {

    render() {
        return (
           
            <View style = {styles.container} >
              
                <Text style = {{padding : 25,height: 50}}/>
                <Text style = {{textAlign:'center', padding : 10, fontSize: 20}} >Register</Text>
                <Input
                    placeholder=' Name - Surname`'
                    leftIcon={
                    <Icon
                    name='address-card'
                    size={24}
                    color='grey'
                    />    
                }
                />
               
                <Input
                    placeholder=' User Name'
                    leftIcon={
                    <Icon
                    name='user'
                    size={24}
                    color='grey'
                    />    
                }
                />
                <Input
                    placeholder=' Password'
                    leftIcon={
                    <Icon
                    name='key'
                    size={24}
                    color='grey'
                    />    
                }
                />
                 <Input
                    placeholder=' Please enter Password again'
                    leftIcon={
                    <Icon
                    name='key'
                    size={24}
                    color='grey'
                    />    
                }
                />

                <Text style = {{padding : 25,height: 50}}/>

                <Button
                title = "Register"
                //  icon={
                //      <Icon
                //      name="unlock"
                //      size={15}
                //      color="white"
                //      />}
                />
              
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
        justifyContent: 'space-between'
      },
    button: {
        backgroundColor: '#2089DC', width: "50%", borderRadius: 12, justifyContent: 'center', alignItems: 'center',paddingVertical: 10
    },
});
