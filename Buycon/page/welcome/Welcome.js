import React, { Component } from 'react'
import {View, Text, TextInput, Image,StyleSheet} from 'react-native'
import { Input, Button, Card} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Login from '../login/loginPage'

export default class componentName extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image
                style={styles.logo}
                source={require('../Images/logo.png')}
                />
                <View>
                <Login/>
                </View>
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        
        backgroundColor: '#e5e5e5'
      },
    logo: {
        width: 200,
        height: 200,
    },
});
