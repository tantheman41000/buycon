import { createStackNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';


import Chooser from './page/chooseOption/Chooser';
import Welcome from './page/welcome/Welcome'
import Register from './page/register/Register'
import InputRestaurant from './page/Restaurant/InputRestaurant';
import InputMenu from './page/Restaurant/InputMenu'
import AddBeacon from './page/Beacon/AddBeacon'

const Options = createStackNavigator(
  {
    Choise : Chooser
  }
)

const WelcomePage = createStackNavigator(
  {
    LoginRoute : Welcome
  }
)

const Registers = createStackNavigator(
  {
    LoginRoute : Welcome,
    CreateAccount : Register
    
  }
)

const RRest = createStackNavigator(
  {
    Choise : Chooser,
    RgRest : InputRestaurant
  }
)

const ADish = createStackNavigator(
  {
    Choise : Chooser,
    RgRest : InputRestaurant,
    AdDish : InputMenu
  }
)

const ABeacon = createStackNavigator(
  {
    Choise : Chooser,
    adddBeacon : AddBeacon
  }
)

export default createAppContainer(createSwitchNavigator(
  {
    Greeting: WelcomePage,
    Option: Options,
    SignUp : Registers,
    RegisterRestuarant : RRest,
    AddDishes : ADish,
    RegisterBeacon : ABeacon
  }
))
