import React, { Component } from 'react'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Chooser from './page/chooseOption/Chooser';
import Welcome from './page/welcome/Welcome'
import {View, Text, TextInput} from 'react-native'


const RootStack = createStackNavigator(
    {
        Home : Welcome
        
    },
    {
        initialRouteName: 'Home'
    }
);

const AppContainer = createAppContainer(RootStack);
console.log(AppContainer)

export default class componentName extends Component {
    render() {
        return (
            <View>
                <AppContainer/>
                <Text>hello</Text>
            </View>
            
        )
    }
}
