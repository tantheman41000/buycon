import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IBeacon } from '@ionic-native/ibeacon';
import { LocalNotifications } from '@ionic-native/local-notifications';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,private ibeacon: IBeacon,private localNotifications: LocalNotifications) {

  }

  ionViewWillEnter() {
        // Request permission to use location on iOS
    // this.ibeacon.requestAlwaysAuthorization();
    // create a new delegate and register it with the native layer
    let delegate = this.ibeacon.Delegate();

    // Subscribe to some of the delegate's event handlers
    delegate.didRangeBeaconsInRegion()
      .subscribe(
        data => {
          console.log('didRangeBeaconsInRegion: ', data );
          if (data.beacons.length > 0){
            data.beacons.forEach(beacon => {
              if (beacon.proximity == 'ProximityImmediate'){
                switch(Number(beacon.minor)){
                  case 19641: console.log("this is my beaccon")
                  break;
                }
              }
            })
          }
        },
        error => console.error()
      );
    delegate.didStartMonitoringForRegion()
      .subscribe(
        data => console.log('didStartMonitoringForRegion: ', data),
        error => console.error()
      );
    delegate.didDetermineStateForRegion().subscribe(data =>{
      let state = data.state;
      if (state == 'CLRegionStateInside'){
        this.localNotifications.schedule({
          id: 1,
          title: 'B1',
          text: 'welcome to the jungle',
          trigger: {at: new Date(new Date().getTime())}
        });
      }
    })
    delegate.didEnterRegion()
      .subscribe(
        data => {
          console.log('didEnterRegion: ', data);
          this.localNotifications.schedule({
           id: 1,
           title: 'B1',
           text: 'welcome to the jungle',
           trigger: {at: new Date(new Date().getTime())}
         });
        }
      );

    let beaconRegion = this.ibeacon.BeaconRegion('BeaconA3000','FDA50693-A4E2-4FB1-AFCF-C6EB07647825');

    this.ibeacon.startMonitoringForRegion(beaconRegion)
      .then(
        () => console.log('Native layer received the request to monitoring'),
        error => console.error('Native layer failed to begin monitoring: ', error)
      );
      this.ibeacon.requestStateForRegion(beaconRegion).then(()=>console.log('Request state!'))
  }

}
