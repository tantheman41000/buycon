package senior.project.buyconbackend.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import senior.project.buyconbackend.entity.Dish
import senior.project.buyconbackend.entity.Location
import senior.project.buyconbackend.entity.Menu
import senior.project.buyconbackend.entity.Restaurant
import senior.project.buyconbackend.repository.DishRepository

@Repository
class DishDaoImpl: DishDao {

    @Autowired
    lateinit var dishRepository: DishRepository

    override fun getDishByRestName(name: String): List<Dish> {
        return dishRepository.findByRestaurant_NameContainingIgnoreCase(name)
    }


}