package senior.project.buyconbackend.dao

import org.springframework.stereotype.Repository
import senior.project.buyconbackend.entity.Menu

@Repository
class MenuDaoImpl: MenuDao {
    override fun getMenus(): List<Menu> {
        return mutableListOf(
                Menu("Thai smile"),
                Menu("Hongdee")
        )
    }
}