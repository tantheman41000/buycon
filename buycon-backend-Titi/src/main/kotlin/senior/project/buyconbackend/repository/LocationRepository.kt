package senior.project.buyconbackend.repository

import org.springframework.data.repository.CrudRepository
import senior.project.buyconbackend.entity.Location

interface LocationRepository: CrudRepository<Location,Long> {
}