package senior.project.buyconbackend.repository

import org.springframework.data.repository.CrudRepository
import senior.project.buyconbackend.entity.Dish

interface DishRepository: CrudRepository<Dish, Long> {
    fun findByRestaurant_NameContainingIgnoreCase(name: String): List<Dish>
}