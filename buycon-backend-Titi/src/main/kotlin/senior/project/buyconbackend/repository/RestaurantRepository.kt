package senior.project.buyconbackend.repository

import org.springframework.data.repository.CrudRepository
import senior.project.buyconbackend.entity.Restaurant

interface RestaurantRepository: CrudRepository<Restaurant, Long> {

    fun findByName(name: String): Restaurant

}