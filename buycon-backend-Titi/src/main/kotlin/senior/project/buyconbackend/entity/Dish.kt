package senior.project.buyconbackend.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Dish(var dishName: String,
                var price: Double,
                var image: String,
                var dishDesc: String) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    lateinit var restaurant: Restaurant

    constructor(dishName: String,
                price: Double,
                image: String,
                dishDesc: String,
                restuarant: Restaurant): this(dishName, price, image, dishDesc){
        this.restaurant = restaurant
    }

}