package senior.project.buyconbackend.entity

import javax.persistence.*

@Entity
data class Restaurant(var name: String,
                      var description: String,
                      var image: String) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToOne
    lateinit var location: Location

    @OneToMany
    var dishes = mutableListOf<Dish>()

    constructor(name: String,
                description: String,
                image: String,
                location: Location): this(name, description, image){
        this.location = location
    }
}