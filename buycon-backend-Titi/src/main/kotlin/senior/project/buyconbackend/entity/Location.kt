package senior.project.buyconbackend.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Location(var latitude: String,
                    var longitude: String) {

    @Id
    @GeneratedValue
    var id: Long? = null
}