package senior.project.buyconbackend.entity.dto

data class DishDto(var dishName: String? = null,
                   var price: Double? = null,
                   var image: String? = null,
                   var dishDesc: String? = null
                   ) {
}