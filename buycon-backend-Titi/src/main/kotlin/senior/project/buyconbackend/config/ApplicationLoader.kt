package senior.project.buyconbackend.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import senior.project.buyconbackend.entity.Dish
import senior.project.buyconbackend.entity.Location
import senior.project.buyconbackend.entity.Restaurant
import senior.project.buyconbackend.repository.DishRepository
import senior.project.buyconbackend.repository.LocationRepository
import senior.project.buyconbackend.repository.RestaurantRepository
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner {

    @Autowired
    lateinit var restaurantRepository: RestaurantRepository

    @Autowired
    lateinit var dishRepository: DishRepository

    @Autowired
    lateinit var locationRepository: LocationRepository

    @Transactional
    override fun run(args: ApplicationArguments?){

        var lo1 = locationRepository.save(Location("1234", "5678"))

        var res1 = restaurantRepository.save(Restaurant("thai smile", "tan's restaurant", "img-url"))

        var dish1 = dishRepository.save(Dish("pad thai",45.00,"url","this is pad thai"))
        var dish2 = dishRepository.save(Dish("som tum", 30.00,"url","this is som tum"))

        res1.location = lo1

        res1.dishes.add(dish1)
        dish1.restaurant = res1

        res1.dishes.add(dish2)
        dish2.restaurant = res1



    }
}