package senior.project.buyconbackend.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import senior.project.buyconbackend.service.RestaurantService
import senior.project.buyconbackend.util.MapperUtil

@RestController
class RestaurantController {

    @Autowired
    lateinit var restaurantService: RestaurantService

    @GetMapping("/restaurants")
    fun getRestaurants(): ResponseEntity<Any> {
        val restaurants = restaurantService.getRestaurants()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapRestaurantDto(restaurants))
    }

    @GetMapping("/restaurant/{name}")
    fun getRestaurantByName(@PathVariable("name") name: String): ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapRestaurantDto(restaurantService.getRestaurantByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
}