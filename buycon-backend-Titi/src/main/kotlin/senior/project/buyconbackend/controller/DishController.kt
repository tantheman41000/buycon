package senior.project.buyconbackend.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import senior.project.buyconbackend.service.DishService
import senior.project.buyconbackend.util.MapperUtil

@RestController
class DishController {

    @Autowired
    lateinit var dishService: DishService

    @GetMapping("/dish/{restaurant}")
    fun getDishByRestName(@PathVariable("restaurant") name: String): ResponseEntity<Any>{
        val dishes = dishService.getDishByRestName(name)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapDishDto(dishes))
    }
}