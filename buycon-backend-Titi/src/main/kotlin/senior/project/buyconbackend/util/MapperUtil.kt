package senior.project.buyconbackend.util

import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers
import senior.project.buyconbackend.entity.Dish
import senior.project.buyconbackend.entity.Restaurant
import senior.project.buyconbackend.entity.dto.DishDto
import senior.project.buyconbackend.entity.dto.RestaurantDto

@Mapper(componentModel = "spring")
interface MapperUtil{

    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapDishDto(dish: Dish): DishDto
    fun mapDishDto(dishes: List<Dish>): List<DishDto>

//    fun mapRestaurantDto(restaurant: Restaurant): RestaurantDto
    fun mapRestaurantDto(restaurants: List<Restaurant>): List<RestaurantDto>
    fun mapRestaurantDto(restaurant: Restaurant?):  RestaurantDto?
}