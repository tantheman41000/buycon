package senior.project.buyconbackend.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import senior.project.buyconbackend.dao.MenuDao
import senior.project.buyconbackend.entity.Menu

@Service
class MenuServiceImpl: MenuService {

    @Autowired
    lateinit var menuDao: MenuDao
    override fun getMenus(): List<Menu> {
        return menuDao.getMenus()
    }
}