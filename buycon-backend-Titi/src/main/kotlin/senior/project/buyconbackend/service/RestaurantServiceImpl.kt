package senior.project.buyconbackend.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import senior.project.buyconbackend.dao.RestaurantDao
import senior.project.buyconbackend.entity.Restaurant

@Service
class RestaurantServiceImpl: RestaurantService {

    @Autowired
    lateinit var restaurantDao: RestaurantDao

    override fun getRestaurants(): List<Restaurant> {
        return restaurantDao.getRestaurants()
    }

    override fun getRestaurantByName(name: String): Restaurant? {
        return restaurantDao.getRestaurantByName(name)
    }
}