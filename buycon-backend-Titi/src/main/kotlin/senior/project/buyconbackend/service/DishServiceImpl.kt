package senior.project.buyconbackend.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import senior.project.buyconbackend.dao.DishDao
import senior.project.buyconbackend.entity.Dish

@Service
class DishServiceImpl : DishService {

    @Autowired
    lateinit var dishDao: DishDao

    override fun getDishByRestName(name: String): List<Dish> {
        return dishDao.getDishByRestName(name)
    }


}