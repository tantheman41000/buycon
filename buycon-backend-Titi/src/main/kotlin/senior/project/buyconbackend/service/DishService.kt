package senior.project.buyconbackend.service

import senior.project.buyconbackend.entity.Dish

interface DishService {

    fun getDishByRestName(name: String): List<Dish>
}