package senior.project.buyconbackend.service

import senior.project.buyconbackend.entity.Restaurant

interface RestaurantService {
    fun getRestaurants(): List<Restaurant>
    fun getRestaurantByName(name: String): Restaurant?
}